package org.waterford.ligaaclabs.grading.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.grading.service.model.Grade;
import org.waterford.ligaaclabs.grading.webapp.transport.GradeTO;


@Component
public class GradeTO2GradeConverter implements Converter<GradeTO, Grade> {

    @Override
    public Grade convert(GradeTO gradeTO) {
        Grade grade = new Grade();
        grade.setCourse(gradeTO.getCourse());
        grade.setGrade(gradeTO.getGrade());
        grade.setStudent(gradeTO.getStudent());
        grade.setId(gradeTO.getId());
        return grade;
    }
}
