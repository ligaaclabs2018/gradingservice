package org.waterford.ligaaclabs.grading.webapp.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.waterford.ligaaclabs.grading.webapp.controller")
public class WebappConfiguration {


}
