package org.waterford.ligaaclabs.grading.webapp.controller;

import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.waterford.ligaaclabs.grading.service.GradingService;
import org.waterford.ligaaclabs.grading.service.model.Grade;
import org.waterford.ligaaclabs.grading.webapp.transport.GradeTO;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grades")
public class GradingController {

    @Inject
    private GradingService service;
    @Inject
    private ConversionService conversionService;

    @GetMapping(path = "/{studentId}")
    public ResponseEntity<List<GradeTO>> getGrades(@PathVariable("studentId") Integer studentId) {
        try {
            return new ResponseEntity<>(service.getGrades(studentId)
                    .stream()
                    .map(g -> conversionService.convert(g, GradeTO.class))
                    .collect(Collectors.toList()), HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<GradeTO> saveGrade(@RequestBody GradeTO gradeTo) {
        try {
            Grade grade = service.saveGrade(conversionService.convert(gradeTo, Grade.class));
            GradeTO result = conversionService.convert(grade, GradeTO.class);
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public void heakthCheck() {
    }

}
