package org.waterford.ligaaclabs.grading.webapp.converters.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import javax.inject.Inject;
import java.util.Set;

/*
Will mark this class as a configuration one. Configurations can be made using annotation, implementing configuration
interfaces or by defining bean providing methods ( See @Bean )
*/
@Configuration
@ComponentScan("org.waterford.ligaaclabs.grading.webapp.converters.*")
public class ConvertersConfig {
    @Inject
    private Set<Converter> converters;

    @Bean
    public ConversionService conversionService() {
        ConversionServiceFactoryBean factory = new ConversionServiceFactoryBean();
        factory.setConverters(converters);
        factory.afterPropertiesSet();
        return factory.getObject();
    }
}
