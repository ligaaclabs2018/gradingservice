package org.waterford.ligaaclabs.grading.webapp.transport;

public class CourseTO {
    private int id;
    private SubjectTO subject;
    private String teacher;

    public CourseTO(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SubjectTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectTO subject) {
        this.subject = subject;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

}
