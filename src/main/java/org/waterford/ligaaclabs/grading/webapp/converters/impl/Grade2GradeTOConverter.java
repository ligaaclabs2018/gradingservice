package org.waterford.ligaaclabs.grading.webapp.converters.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.waterford.ligaaclabs.grading.service.model.Grade;
import org.waterford.ligaaclabs.grading.webapp.transport.GradeTO;

@Component
public class Grade2GradeTOConverter implements Converter<Grade, GradeTO> {

    @Override
    public GradeTO convert(Grade grade) {
        GradeTO gradeTO = new GradeTO();
        gradeTO.setCourse(grade.getCourse());
        gradeTO.setGrade(grade.getGrade());
        gradeTO.setStudent(grade.getStudent());
        gradeTO.setId(grade.getId());
        return gradeTO;
    }
}
