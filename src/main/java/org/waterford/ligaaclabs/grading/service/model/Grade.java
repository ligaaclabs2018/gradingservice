package org.waterford.ligaaclabs.grading.service.model;

import org.waterford.ligaaclabs.grading.webapp.transport.CourseTO;
import org.waterford.ligaaclabs.grading.webapp.transport.StudentTO;

public class Grade {

    private String id;
    private CourseTO course;
    private StudentTO student;
    private Double grade;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CourseTO getCourse() {
        return course;
    }

    public void setCourse(CourseTO course) {
        this.course = course;
    }

    public StudentTO getStudent() {
        return student;
    }

    public void setStudent(StudentTO student) {
        this.student = student;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
