package org.waterford.ligaaclabs.grading.service;

import org.waterford.ligaaclabs.grading.service.model.Grade;

import java.util.List;

public interface GradingService {

    Grade saveGrade(Grade grade);

    List<Grade> getGrades(Integer studentId);
}
