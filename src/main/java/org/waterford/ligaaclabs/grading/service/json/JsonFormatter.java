package org.waterford.ligaaclabs.grading.service.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.waterford.ligaaclabs.grading.service.model.Grade;

import java.io.IOException;

public class JsonFormatter {

    public static  String writeJson(Grade grade) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(grade);
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }

    public static Grade readJSON(String json){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, Grade.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
