package org.waterford.ligaaclabs.grading.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import org.springframework.stereotype.Service;
import org.waterford.ligaaclabs.grading.service.GradingService;
import org.waterford.ligaaclabs.grading.service.json.JsonFormatter;
import org.waterford.ligaaclabs.grading.service.model.Grade;

import javax.inject.Inject;
import java.util.*;

@Service
public class GradingServiceImpl implements GradingService {

    @Inject
    private AmazonDynamoDB dynamoDB;
    @Inject
    private AmazonSNSAsync snsAsync;
    private final static String TABLE_NAME = "ligaac_labs_grades";
    private final static String GRADE = "Grade";

    @Override
    public Grade saveGrade(Grade grade) {
        Map<String,AttributeValue> itemValues =  new HashMap<>();
        grade.setId(UUID.randomUUID().toString());
        String studentId = grade.getStudent().getId().toString();
        String timestamp = String.valueOf(System.currentTimeMillis());
        itemValues.put("studentId", new AttributeValue(studentId));
        itemValues.put("timestamp",  new AttributeValue().withN(timestamp));
        itemValues.put(GRADE, new AttributeValue(getAttributeAsJSON(grade)));
        try{
            dynamoDB.putItem(TABLE_NAME, itemValues);
            publishNotification(studentId, timestamp);
        } catch (AmazonServiceException e) {
            throw e;
        }
        return grade;
    }

    private void publishNotification(String studentId, String timestamp) {
        String topicArn = "arn:aws:sns:us-east-1:590613178876:NewGrade";
        String message = studentId + "::" + timestamp;
        PublishRequest publishRequest = new PublishRequest(topicArn, message);
        PublishResult publish = snsAsync.publish(publishRequest);
        System.out.println(publish);
    }


    @Override
    public List<Grade> getGrades(Integer studentId) {
        List<Grade> grades = new ArrayList<>();
        Map<String, AttributeValue> lastKeyEvaluated = null;
        do {
            ScanRequest scanRequest = new ScanRequest()
                            .withTableName(TABLE_NAME)
                            .withLimit(10)
                            .withExclusiveStartKey(lastKeyEvaluated);

            ScanResult result = dynamoDB.scan(scanRequest);

            result.getItems().forEach(item->{
                String gradeItem = item.get(GRADE).toString();
                Grade grade = JsonFormatter.readJSON(gradeItem.substring(3, gradeItem.length() - 2));
                if(studentId.equals(grade.getStudent().getId())) {
                    grades.add(grade);
                }
            });
            lastKeyEvaluated = result.getLastEvaluatedKey();
            
            } while (lastKeyEvaluated != null);

       return grades;
    }

    private String getAttributeAsJSON(Grade grade){
           return JsonFormatter.writeJson(grade);
    }
}
