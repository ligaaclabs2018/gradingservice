package org.waterford.ligaaclabs.grading.service.config;

import com.amazonaws.auth.*;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.waterford.ligaaclabs.grading.service")
public class ServiceConfiguration {
    @Value("${aws.access.key}")
    private String awsAccessKey;

    @Value("${aws.secret.key}")
    private String awsSecretKey;

    @Value("${aws.region}")
    private String awsRegion;

    @Bean
    public AWSCredentialsProvider credentialsProvider() {
        DefaultAWSCredentialsProviderChain defaultAWSCredentialsProviderChain = new DefaultAWSCredentialsProviderChain();
        AWSStaticCredentialsProvider staticCredentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
        return new AWSCredentialsProviderChain(staticCredentialsProvider, defaultAWSCredentialsProviderChain);
    }

    @Bean
    public AmazonDynamoDB dynamoDB() {
        AmazonDynamoDBClientBuilder amazonDynamoDB = AmazonDynamoDBClientBuilder.standard();

        amazonDynamoDB.setCredentials(credentialsProvider());
        amazonDynamoDB.setRegion(awsRegion);

        return amazonDynamoDB.build();
    }

    @Bean
    public AmazonSNSAsync snsClient() {
        AmazonSNSAsyncClientBuilder snsBuilder = AmazonSNSAsyncClientBuilder.standard();
        snsBuilder.setCredentials(credentialsProvider());
        snsBuilder.setRegion(awsRegion);

        return snsBuilder.build();
    }
}
